$(document).foundation();

$(".tasklist-item-delete").click(function(e){
  element = $( this );
  line = element.parent();
  taskid = element.data('delete')
  $.ajax({
  url: "delete.php?id="+taskid,
  }).done(function(data) {
    if(data = '1'){
      element.parent(".tasklist-item").remove();
    }

  });
});

$(".tasklist-item-status").click(function(e){
  $this = $( this );
  $done = $this.data('status');
  $.ajax({
  url: "done.php?id="+$this.data('id')+"&status="+$done,
  }).done(function() {
    $this.data('status',$done==1?'':1);
    $this.parents("li").toggleClass('is-done');
    $this.children('a').children('i').toggleClass('fa-check-circle');
    $this.children('a').children('i').toggleClass('fa-square-o');
  });
});


$(".userlist-item-delete").click(function(e){
  element = $( this );
  line = element.parent();
  taskid = element.data('delete-user')
  $.ajax({
  url: "delete_user.php?id="+taskid,
  }).done(function(data) {
    if(data = '1'){
      element.parent(".userlist-item").remove();
    }

  });
});

$(document).on('click', '.sb-status', {}, function(e) {
  $this = $(this);
  $status = 'status=' + $this.data('status');
  $('.sb-status').removeClass('selected');
  $this.addClass('selected');

  $.ajax({
    url: "tasklist.php?" + $status
  }).done(function(data) {
    $('.off-canvas-content').html(data);
  });
});
