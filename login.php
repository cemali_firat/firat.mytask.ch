<?php
  require_once('config.php');
?>

<html class="no-js" lang="en">
  <body>
    <?php require_once 'head.php'; ?>
    <?php require_once 'header.php'; ?>
    <?php require_once 'canvas.php';?>
        <main class="row">
          <form method="post" action="logme.php">
            <ul class="edit-ul">
              <li>
                <label>Username : </label>
                <textarea name="name" id="name"  rows="1" cols="10"></textarea>
              </li>
              <li>
                <label>Password : </label>
                <textarea type="password" id="password" name="password"  rows="1" cols="10"></textarea>
              </li>
            </ul>
              <input class="button-submit" type="submit" value="Log in"/>
          </form>
        </main>
      </div>
    </div>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
  <?php require_once 'footer.php'; ?>
</html>
