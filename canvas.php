<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft1" data-off-canvas>
    <ul>
      <h4 class="sbtitle">Status Filter</h2>
      <ul>
        <li><a href="?status=all">All</a></li>
        <li><a href="?status=open">Open</a></li>
        <li><a href="?status=closed">Close</a></li></br>
      </ul>
      <li>
        <h4><a href="index.php">Home</a></h4></br>
      </li>
      <li>
        <h4><a href="user.php"> All users</a></h4></br>
      </li>
      <li>
        <h4><a href="edit.php"> Add task</a></h4></br>
      </li>
      <li>
        <h4><a href="edit_user.php"> Add user</a></h4></br>
      </li>
      <li>
        <h4><a href="about.php">About</a></h4></br>
      </li>
      <li>
        <h4><a href="user_manual.pdf">User manual</a></h4></br>
      </li>
      <li>
        <h4><a href="technical_manual.pdf">Technical manual</a></h4></br>
      </li>
      <li>
        <h4><a href="cv.php">My CV</a></h4></br>
      </li>
      <li>
        <h4><a href="logout.php"> Log out</a></h4></br>
      </li>
    </ul>
  </div>
  <div class="off-canvas-content" data-off-canvas-content>
