<?php
  require_once('config.php');
?>

<html class="no-js" lang="en">
  <body>
    <?php require_once 'head.php'; ?>
    <?php require_once 'header.php'; ?>
    <?php require_once 'canvas.php';?>
        <main class="row">
          <section class="row">
            <h2>About this project</h2>

            <p>
              Ce projet a été implémenté dans le cadre du cours "i-communication" de la première année de l'HEIA-FR, Haute Ecole d'Ingénieur et d'Architecte de Fribourg, en filière "Internet et communication"
            </p>

            <p>
              Le but principale était de prendre en main les différentes technologies webs, tel que: HTML, CSS, JS, PHP mais également le framework foundation et bien d'autres,...
            </p>

            <p>
              Le contexte était de créer un application de gestion de tâches multi-utilisateurs, qui pourrait servir en entreprise pour la gestion de projets IT.
            </p>
          </section>

        </main>
      </div>
    </div>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
  <?php require_once 'footer.php'; ?>
</html>
