<!doctype html>
<html class="no-js" lang="en">
  <?php require_once 'head.php';?>
  <body>
    <?php require_once 'config.php';?>
    <?php require_once 'security.php';?>
    <?php require_once 'header.php';?>
    <?php require_once 'canvas.php';?>
    
            <main class="row ">
                <ul class="userlist">
                  <li class="row userlist-item">
                    <span class="userlist-item-id">ID</span>
                    <span class="userlist-item-name">Name</span>
                    <span class="userlist-item-email">E-mail</span>
                    <span class="userlist-item-delete">delete</span>
                  </li>
                  <?php foreach ($data_user as $row) : ?>
                  <li class="row userlist-item userlist-li-data">
                      <a href="edit_user.php?id=<?php echo $row['id']; ?>">
                        <span class="userlist-item-id"><?php echo $row['id']?></span>
                        <span class="userlist-item-name"><?php echo $row['name']?></span>
                        <span class="userlist-item-email"><?php echo $row['email']?></span>
                      </a>
                    <span data-delete-user="<?php echo $row['id'] ?>" class="userlist-item-delete">
                      <a href="#">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </span>
                  </li>
                  <?php endForeach;?>
                </ul>

              <a class="button-add" href="edit_user.php"><i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i></a>
            </main>
          </div>
        </div>
    <?php require_once './footer.php'; ?>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="./js/app.js"></script>
  </body>
</html>
