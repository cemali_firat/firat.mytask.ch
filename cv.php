<!doctype html>
<html lang="fr-CH">
  <?php require_once 'head.php';?>
	<body>
    <?php require_once 'config.php';?>
    <?php require_once 'security.php';?>
    <?php require_once 'header.php';?>
    <?php require_once 'canvas.php';?>
		<div class="info_bar">
			<ul>
				<li><a href="#experiences">Expériences</a></li>
				<li><a href="#formation">Formation</a></li>
				<li><a href="#projets">Projets EMF</a></li>
				<li><a href="#competences">Compétences</a></li>
				<li><a href="#interets">Intérêts</a></li>
			</ul>
		</div>
		<div class="container">
			<div class="leftside">
				<img class="image" src="img/image.jpg"/>
				<h3>Firat Cemali</h3>
				<h3>Rue de la Sionge 59</h3>
				<h3>1630 Bulle</h3>
				<h3>cemali.firat@edu.hefr.ch</h3>
				<div class="cadre">
					<h4>Informations Personnelles</h4>
				</div>
				<h4>Qualités personnelles :</h4>
				<ul>
					<li>Sociale</li>
					<li>Souriant</li>
					<li>Honnête</li>
					<li>Ambitieux</li>
				</ul>
				<h4>Qualités professionnelles :</h4>
				<ul>
					<li>Flexible</li>
					<li>Serviable</li>
					<li>Efficace</li>
					<li>Responsable</li>
				</ul>
				<div class="cadre">
					<h4>Compétences Personnelles</h4>
				</div>
				<h4>Langues</h4>
				<div class="tab_langue">
					<div class="tab_langue_left">
						<a>Allemand: </a>
					</div>
					<div class="tab_langue_right">
						<table>
   							<tr>
      							<td class="tab_ok"/><td class="tab_ok"/><td class="tab_ok"/><td class="tab_ko"/><td class="tab_ko"/><td class="tab_ko"/>
   							</tr>
						</table>
					</div>
					<div class="tab_langue_left">
						<a>Anglais: </a>
					</div>
					<div class="tab_langue_right">
						<table>
   							<tr>
      							<td class="tab_ok"/><td class="tab_ok"/><td class="tab_ok"/><td class="tab_ok"/><td class="tab_ko"/><td class="tab_ko"/>
   							</tr>
						</table>
					</div>
				</div>
				<h4>Compétences en communication :</h4>
				<ul>
					<li>Bon esprit d’équipe acquis durant ma formation</li>
					<li>Bonne capacité à s’adapter à tous genres de situations</li>
				</ul>
				<h4>Compétences en organisation :</h4>
				<ul>
					<li>Bonne capacité d’organisation acquise durant ma formation</li>
					<li>Bonne capacité à travailler en équipe</li>
				</ul>
			</div>
			<div class="rightside">
				<h1>Curriculum Vitae</h1>
				<h2> Ingénieur en Informatique</h2>
				<div class="cadre">
					<h4 id="experiences">Expériences professionnelles</h4>
				</div>
				<p class="annee">(Été 2015 -- 2016)</p>
				<p class="stitre">CISEL Informatique SA Matran</p>
				<p class="sstitre">Stagiaire informaticien</p><br/>

				<p class="annee">(Août 2014  –  Décembre 2014)</p>
				<p class="stitre">EPAI | CSCR | CSMI Fribourg</p>
				<p class="sstitre">Technicien système</p><br/>

				<p class="annee">(Été 2014)</p>
				<p class="stitre">Hôtel-Restaurant Le Gruyérien Morlon</p>
				<p class="sstitre">Assistant cuisinier</p><br/>

				<p class="annee">(Été 2014)</p>
				<p class="stitre">EPAI | CSCR | CSMI Fribourg</p>
				<p class="sstitre">Technicien système</p>

				<div class="cadre">
					<h4 id="formation">Formation</h4>
				</div>
				<p class="annee">(Août 2015  –  Actuellement)</p>
				<p class="stitre">Bachelor of Science HES-SO en informatique </p>
				<p class="sstitre">Membre de l’université des Sciences appliquées de la Suisse Occidentale Haute école d’ingénierie et d’architecture Fribourg </p><br/>

				<p class="annee">(Août 2011  –   Juillet  2015)</p>
				<p class="stitre">CFC informaticien généraliste</p>
				<p class="sstitre">Maturité technique acquise sur 3 ans Ecole des Métiers, Fribourg</p><br/>

				<p class="annee">(Août 2008  –   Juillet  2011)</p>
				<p class="stitre">Diplôme de fin de scolarité obligatoire </p>
				<p class="sstitre">Cycle d’orientation de Bulle</p>


				<div class="cadre">
					<h4 id="projets">Projets, Ecole des Métiers</h4>
				</div>
				<h4 class="stitre">Projets réseaux :</h4>
				<ul>
					<li class="sstitre">Création d’un réseau moyen (~16 routers, 16 switches, 1 firewall).</li>
					<li class="sstitre">Gestion d’un grand réseau (Mandat de 4 mois)</li>
				</ul>
				<h4 class="stitre">Projets JAVA :</h4>
				<ul>
					<li class="sstitre">Création d’un projet java avec une Leap Motion et un robot K-Junior. </li>
				</ul>
				<h4 class="stitre">Projets WEB :</h4>
				<ul>
					<li class="sstitre">Développement d’un site web statique (première année de formation), site web dynamique (deuxième année) en utilisant PHP et Flex, MySQL et Zend Framework (troisième année).</li>
				</ul>
				<h4 class="stitre">Projets LINUX :</h4>
				<ul>
					<li class="sstitre">Implémentation d’un serveur web Apache. Créations de sites web (Virtual Host) et la gestion des accès aux ressources. Configuration de serveur (définition des droits des utilisateurs).</li>
				</ul>
				<h4 class="stitre">Projets système :</h4>
				<ul>
					<li class="sstitre">Gestion d’un domaine Active Directory (Windows 2008 R2 et 2012 R2). Gestion des accès et authentification        des ressources pour les utilisateurs..</li>
				</ul>
				<h4 class="stitre">Projets base de données :</h4>
				<ul>
					<li class="sstitre">Gestion de bases de données MySQL avec plusieurs tables. Requêtes afin d’obtenir le résultat sur plusieurs tables (utilisation des « joins »).</li>
				</ul>
				<div class="cadre">
					<h4 id="competences">Compétences informatiques</h4>
				</div>
				<h4 class="stitre">Système d’exploitation</h4>
				<ul>
					<li class="sstitre">Windows, Linux et Apple.</li>
				</ul>
				<h4 class="stitre">Langages de programmation</h4>
				<ul>
					<li class="sstitre">Java, PHP (JavaScript), Flex (XML, HTML, CSS)</li>
				</ul>
				<h4 class="stitre">Technologies réseaux / Protocoles: </h4>
				<ul>
					<li class="sstitre">RIP, RIPv2, OSPF, EIGRP, DHCP, DNS, TCP/IP VLAN, NAT/PAT, Active Directory, router, switch, server...</li>
				</ul>
				<h4 class="stitre">Base de données</h4>
				<ul>
					<li class="sstitre">MySQL</li>
				</ul>
				<h4 class="stitre">Hardware</h4>
				<ul>
					<li class="sstitre">Connaissance générale, assembler/désassembler un PC et réparation..</li>
				</ul>
				<h4 class="stitre">Software utilisé</h4>
				<ul>
					<li class="sstitre">Microsoft Office (Word, Excel, PowerPoint etc.), Netbeans, Dreamweaver, Adobe Flash Builder, Putty terminal, WireShark, Packet Tracer, VMware Workstation, WampServer, Workbench (MySQL), Photoshop, Enterprise Architect, server.app (Apple) et plus…</li>
				</ul>
				<div class="cadre">
					<h4 id="interets">Intérêts personnels</h4>
				</div>
				<h4 class="stitre">Acquérir de nouvelles compétences en Informatique</h4>
				<h4 class="stitre">Ecouter de la musique</h4>
				<h4 class="stitre">Fitness</h4>
				<h4 class="stitre">Sport d’équipes tel que le football, basketball, etc.</h4>
			</div>
		</div>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="./js/app.js"></script>
	</body>
</html>
