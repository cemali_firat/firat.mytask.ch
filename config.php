<?php
session_start();
  $username = "root";
  $password = "pass";

  $db = new PDO('mysql:host=localhost;dbname=newtask;charset=utf8mb4', $username, $password);

$query = "SELECT task.*, author.name as author, assignee.name as assignee_name, executer.name as executer_name FROM task
                    INNER JOIN user author ON task.created_by = author.id
                    LEFT JOIN user assignee ON task.assigned_to = assignee.id
                    LEFT JOIN user executer ON task.done_by = executer.id";

if(isset($_GET['status'])){
  if($_GET['status']=='closed' ){
    $query .= " where task.status='closed'";
  }elseif ($_GET['status']=='open') {
    $query .= " where task.status='open'";
  }
}

if (isset($_GET['sorting'])) {
  if ($_GET['sorting']=='id' && $_GET['idOrder']==1) {
    $query .= " order by id ASC";
  }elseif ($_GET['sorting']=='id') {
    $query .= " order by id DESC";
  }
}

if (isset($_GET['sorting'])) {
  if ($_GET['sorting']=='date' && $_GET['dateOrder']==1) {
    $query .= " order by created_by ASC";
  }elseif ($_GET['sorting']=='date') {
    $query .= " order by created_by DESC";
  }
}

  $stmt = $db->query($query);

  $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

  $stmt_user = $db->query("SELECT * FROM user");
  $data_user = $stmt_user->fetchAll(PDO::FETCH_ASSOC);
?>
