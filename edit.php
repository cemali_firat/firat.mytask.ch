<?php
require_once('config.php');

$modify = FALSE;



if(isset($_REQUEST['id'])){
  $modify = TRUE;
  $id = $_REQUEST['id'];
  $stmt = $db->prepare("SELECT task.*, author.name as author, assignee.name as assignee_name, executer.name as executer_name FROM task
                      INNER JOIN user author ON task.created_by = author.id
                      LEFT JOIN user assignee ON task.assigned_to = assignee.id
                      LEFT JOIN user executer ON task.done_by = executer.id where task.id = ? ");
  $stmt->execute(array($id));
  $data = $stmt->fetchAll();
  $description = $data[0]['description'];
  $due_at = $data[0]['due_at'];
  $assigned_to = $data[0]['assigned_to'];
  $priority = $data[0]['priority'];
  $status = $data[0]['status'];
  $done_by = $data[0]['done_by'];
  $author = $data[0]['author'];
  $assignee_name = $data[0]['assignee_name'];
  $executer_name = $data[0]['executer_name'];

}
?>

<html class="no-js" lang="en">
  <body>
    <?php require_once 'head.php'; ?>
    <?php require_once 'header.php'; ?>
    <?php require_once 'canvas.php';?>

        <main class="row">
          <form method="post" action="update.php">
            <input type="hidden" name="id" id="id" value="<?php echo $modify?$id:'' ;?>"/>
            <ul class="edit-ul">
              <li>
                <label>Description : </label>
                <textarea name="description"  rows="2" cols="70"><?php echo $modify?$description:''; ?></textarea>
              </li>
              <li>
                <label>Author : </label>
                <textarea name="author"  rows="2" cols="70"><?php echo $modify?$author:''; ?></textarea>
              </li>
              <li>
                <label>Assignee name : </label>
                <textarea name="assignee_name"  rows="2" cols="70"><?php echo $modify?$assignee_name:''; ?></textarea>
              </li>
              <li>
                <label>Executer name : </label>
                <textarea name="executer_name"  rows="2" cols="70"><?php echo $modify?$executer_name:''; ?></textarea>
              </li>
              <li>
                <label>Due date : </label>
                <input name="due_at" type="date" value="<?php echo $modify?$due_at:''; ?>"/>
              </li>
              <li>
                <label>Priority :</label>
                <input name="priority" id="priority" type="range" min="1" max="4" step="1" oninput="showVal(this.value)" onchange="showVal(this.value)" value="<?php echo $modify?$priority:'1'; ?>"/>
                <label id="lpriority" name="lpriority"><?php echo $modify?$priority:''; ?></label>
                  <script>
                    var p = document.getElementById("priority"),
                    res = document.getElementById("lpriority");
                    p.addEventListener("input", function() {
                      res.innerHTML =  p.value;
                    }, false);
                  </script>
              </li>
              <li>
                <label>Status :</label>
                  <select name="status">
                    <option value="Open">Open</option>
                    <option value="close" <?php if($status='close') echo 'selected="selected"'; ?>>Close</option>
                  </select>
              </li>
            </ul>
              <input class="button-edit-submit" type="submit" value="Submit"/>
          </form>
        </main>
      </div> <!-- end div for canvas-content !-->
    </div> <!-- end div for canvas !-->
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
  <?php require_once 'footer.php'; ?>
</html>
