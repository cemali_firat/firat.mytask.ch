<!doctype html>
<html class="no-js" lang="en">
  <?php require_once 'head.php';?>
  <body>
    <?php require_once 'config.php';?>
    <?php require_once 'security.php';?>
    <?php require_once 'header.php';?>
    <?php require_once 'canvas.php';?>
            <main class="row ">
                <ul class="tasklist">
                  <li class="row tasklist-item">
                    <span class="tasklist-item-id">ID<a href="?sorting=id&idOrder=<?php echo isset($_GET['idOrder'])?!$_GET['idOrder']:1; ?>"><i class="fa fa-sort" aria-hidden="true"></i></a> </span>
                    <span class="tasklist-item-description">Description</span>
                    <span class="hide-for-medium-only hide-for-small-only tasklist-item-date">date <a href="?sorting=date&dateOrder=<?php echo isset($_GET['dateOrder'])?!$_GET['dateOrder']:1; ?>"><i class="fa fa-sort" aria-hidden="true"></i></a> </span>
                    <span class="hide-for-small-only tasklist-item-due">due</span>
                    <span class="hide-for-medium-only hide-for-small-only tasklist-item-created_by">author</span>
                    <span class="hide-for-medium-only hide-for-small-only tasklist-item-assigned_to">assignee</span>
                    <span class="hide-for-medium-only hide-for-small-only tasklist-item-done_by">executor</span>
                    <span class="hide-for-medium-only hide-for-small-only tasklist-item-priority">Priority</span>
                    <span class="hide-for-small-only tasklist-item-status">status</span>
                    <span class="tasklist-item-delete">delete</span>
                  </li>
                  <?php foreach ($data as $row) : ?>
                    <?php $done = $row['status'] == 'closed'; ?>
                  <li class="row tasklist-item tasklist-li-data <?php if($row['status'] == 'closed') :?> is-done<?php endif;?>">
                    <a href="edit.php?id=<?php echo $row['id']; ?>">
                      <span class="tasklist-item-id"><?php echo $row['id']?></span>
                      <span class="tasklist-item-description"><?php echo $row['description']?></span>
                      <span class="hide-for-medium-only hide-for-small-only tasklist-item-date"><?php echo $row['created_at']?></span>
                      <span class="hide-for-small-only tasklist-item-due"><?php echo $row['due_at']?></span>
                      <span class="hide-for-medium-only hide-for-small-only tasklist-item-created_by"><?php echo $row['author']?></span>
                      <span class="hide-for-medium-only hide-for-small-only tasklist-item-assigned_to"><?php echo $row['assignee_name']?></span>
                      <span class="hide-for-medium-only hide-for-small-only tasklist-item-done_by"><?php echo $row['executer_name']?></span>
                      <span class="hide-for-medium-only hide-for-small-only tasklist-item-priority"><?php echo $row['priority']?></span>
                    </a>
                    <span data-status="<?php echo $row['status'] ?>" data-id="<?php echo $row['id'] ?>" class="hide-for-small-only tasklist-item-status">
                      <a href="#">
                        <?php echo ($done)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-square-o"></i>'; ?>
                      </a>
                    </span>
                    <span data-delete="<?php echo $row['id'] ?>" class="tasklist-item-delete">
                      <a href="#"><i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </span>
                  </li>
                  <?php endForeach;?>
                </ul>

              <a class="hide-for-small-only hide-for-medium-only" href="edit.php"><i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i></a>
            </main>
          </div> <!-- end div for canvas-content !-->
        </div> <!-- end div for canvas !-->
    <?php require_once './footer.php'; ?>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="./js/app.js"></script>
  </body>
</html>
