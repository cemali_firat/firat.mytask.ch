<?php
require_once('config.php');

$modify = FALSE;

if(isset($_REQUEST['id'])){
  $id = $_REQUEST['id'];
  $modify = TRUE;
  $stmt_user = $db->prepare("SELECT * FROM user where user.id = ?");
  $stmt_user->execute(array($id));
  $data_user = $stmt_user->fetchAll();
  $name = $data_user[0]['name'];
  $email = $data_user[0]['email'];
  $password = $data_user[0]['password'];


}
?>

<html class="no-js" lang="en">
  <body>
    <?php require_once 'head.php'; ?>
    <?php require_once 'header.php'; ?>
    <?php require_once 'canvas.php';?>
        <main class="row">
          <form method="post" action="update_user.php">
            <input type="hidden" name="id" id="id" value="<?php echo $modify?$id:'' ;?>"/>
            <ul class="edit-ul">
              <li>
                <label>Name : </label>
                <textarea name="name"  rows="2" cols="70"><?php echo $modify?$name:''; ?></textarea>
              </li>
              <li>
                <label>E-Mail : </label>
                <textarea name="email"  rows="2" cols="70"><?php echo $modify?$email:''; ?></textarea>
              </li>
              <li>
                <label>Password : </label>
                <textarea name="password"  rows="2" cols="70"><?php echo $modify?$password:''; ?></textarea>
              </li>
            </ul>
              <input class="button-edit-submit" type="submit" value="Submit"/>
          </form>
        </main>
      </div>
    </div>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
  <?php require_once 'footer.php'; ?>
</html>
