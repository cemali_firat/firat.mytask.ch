<?php
  require_once 'config.php';

  $status = ($_REQUEST['status']=='open')?'closed':'open';

  $stmt = $db->prepare("update task set status=? where id=?");
  $stmt->execute(array($status, $_REQUEST['id']));

?>
